# hacking course

## Topics

- wireshark (tcp/ip, udp, etc)
- curl
- nmap
- ping/traceroute
- whois/dig
- dirb (talk about wordlists)
- nc
- ssh (talk about modes of authentication)

### Networking

- tcp/ip (mention IETF, RFC)
- bgp (awesome!)
- udp
- routers
- ports

### Cryptographish topics

- block ciphers (good beginner topic)
- pgp/gpg

### Build a fully functional and self-contained computer

- Raspberry Pi 3
- Display?
- Laser cutting for cases
- Possible side quest for adults: soldering introduction (BYOKeyboards)

## Guidelines

- Less talking, more hands on
- Open source content
- Never make assumptions
- Encourage discovery/exploration

## Concrete-ish lesson plans

- Build a Nix workstation on a Raspberry
- Build a chat server
- Breach a vulnerable website
- Crack a password.. or 2... or 3 (hashes, rainbow tables, salting etc)
- Setup a WiFi roaming at home.. improve signaling etc with multiple APs
- Securing WiFi, audit etc
- DDoS thyself... brute forcing as a general concept, defensive
- Securing web forms... SQL injection? Not sanitizing your inputs..CSRF/XSS attacks
